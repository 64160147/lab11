package com.supanut.week11;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println();
        Bat bat = new Bat("I AM BATMAN!");
        bat.eat();
        bat.sleep();
        bat.fly();
        bat.takeoff();
        bat.landing();
        System.out.println();

        Fish fish = new Fish("Stop");
        fish.eat();
        fish.sleep();
        fish.swim();
        System.out.println();

        Plane plane = new Plane("NeverGonnaGiveYouUp!!!", "MaxnaNO!!!");
        plane.fly();
        plane.takeoff();
        plane.landing();
        System.out.println();

        System.out.println();
        Bird bird = new Bird("TongChai");
        bird.eat();
        bird.sleep();
        bird.fly();
        bird.takeoff();
        bird.landing();
        System.out.println();

        Submarine submarine = new Submarine("Over9000", "Nanomachines, son!");
        submarine.swim();
        System.out.println();

        Snake snake = new Snake("Solid Snake");
        snake.eat();
        snake.sleep();
        snake.crawl();
        System.out.println();

        W_Human Human = new W_Human("DaengDoubleGuitar");
        Human.eat();
        Human.sleep();
        Human.walk();
        Human.run();
        System.out.println();

        W_Rat Rat = new W_Rat("Jerry");
        Rat.eat();
        Rat.sleep();
        Rat.walk();
        Rat.run();
        System.out.println();

        W_Cat Cat = new W_Cat("Tom");
        Cat.eat();
        Cat.sleep();
        Cat.walk();
        Cat.run();
        System.out.println();

        W_Dog Dog = new W_Dog("Tu");
        Dog.eat();
        Dog.sleep();
        Dog.walk();
        Dog.run();
        System.out.println();

        Crocodile crocodile = new Crocodile("Loki");
        crocodile.eat();
        crocodile.sleep();
        crocodile.crawl();
        crocodile.swim();
        System.out.println();


        System.out.println();
        FlyAble flyAbleObjects[] = {bat,bird, plane};
        for(int i=0; i<flyAbleObjects.length;i++) {
            flyAbleObjects[i].fly();
            flyAbleObjects[i].takeoff();
            flyAbleObjects[i].landing();
        }
        System.out.println();

        System.out.println();
        SwimAble swimAbleObjects[] = {fish,crocodile,submarine};
        for(int i=0; i<swimAbleObjects.length;i++) {
            swimAbleObjects[i].swim();
        }
        System.out.println();

        System.out.println();
        CrawlAble crawlAbleObjects[] = {snake,crocodile};
        for(int i=0; i<crawlAbleObjects.length;i++) {
            crawlAbleObjects[i].crawl();
        }
        System.out.println();

        System.out.println();
        WalkAble walklAbleObjects[] = {Human,Rat,Cat,Dog};
        for(int i=0; i<walklAbleObjects.length;i++) {
            walklAbleObjects[i].walk();
            walklAbleObjects[i].run();
        }
        System.out.println();
    }
}
