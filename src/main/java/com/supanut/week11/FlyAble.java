package com.supanut.week11;

public interface FlyAble {
    public void fly();
    public void landing();
    public void takeoff();
}
