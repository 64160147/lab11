package com.supanut.week11;

public interface WalkAble {
    public void walk();
    public void run();
}
