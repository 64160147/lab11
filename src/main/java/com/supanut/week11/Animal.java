package com.supanut.week11;

public abstract class Animal {
    private String name;
    private int numberOfLag;
    public Animal(String name,int numberOfLag) {
        this.name = name;
        this.numberOfLag = numberOfLag;
    }
    public String getName() {
        return name;
    }
    public int getnumberOfLag() {
        return numberOfLag;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setNumberOfLeg(int numberOfLag) {
        this.numberOfLag = numberOfLag;
    } 
    @Override
    public String toString() {
        return "Animal (" + name + ") has " + numberOfLag + " lags";
    }
    
    public abstract void eat();
    public abstract void sleep();
}
