package com.supanut.week11;

public class W_Human extends Animal implements WalkAble{
    public W_Human(String name) {
        super(name,2);
    }
    @Override
    public void eat() {
      System.out.println(this.toString() + " eat.");
    }

    @Override
    public void sleep() {
       System.out.println(this.toString() + " sleep.");
    }

    @Override
    public String toString() {
        return "Human("+this.getName()+")";
    }
    
    @Override
    public void walk() {  
        System.out.println(this.toString() + " walk.");
    }
    @Override
    public void run() {  
        System.out.println(this.toString() + " run.");
    }
}
